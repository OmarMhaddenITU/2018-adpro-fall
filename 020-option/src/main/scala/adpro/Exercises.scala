// Advanced Programming, A. Wąsowski, IT University of Copenhagen
//
// Group number: 15
//
// AUTHOR1: Christian Esbensen
// TIME1: 2h <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: Omar Mhadden
// TIME2: 2h <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// This file is compiled with 'sbt compile' and tested with 'sbt test'.
//
// The file shall always compile and run after you are done with each exercise
// (if you do them in order).  Please compile and test frequently. Of course,
// some tests will be failing until you finish. Only hand in a solution that
// compiles and where tests pass for all parts that you finished.    The tests
// will fail for unfnished parts.  Comment such out.

package adpro

// Exercise  1

/* We create OrderedPoint as a trait instead of a class, so we can mix it into
 * Points (this allows to use java.awt.Point constructors without
 * reimplementing them). As constructors are not inherited, We would have to
 * reimplement them in the subclass, if classes not traits are used.  This is
 * not a problem if I mix in a trait construction time. */

trait OrderedPoint extends scala.math.Ordered[java.awt.Point] {

  this: java.awt.Point =>

  override def compare (that: java.awt.Point): Int = {
    val xDiff = this.x - that.x
    val yDiff = this.y - that.y
    if (xDiff != 0) xDiff else if (yDiff != 0) yDiff else 0
  }

}

// Try the following (and similar) tests in the repl (sbt console):
// val p = new java.awt.Point(0,1) with OrderedPoint
// val q = new java.awt.Point(0,2) with OrderedPoint
// assert(p < q)

// Chapter 3


sealed trait Tree[+A]
case class Leaf[A] (value: A) extends Tree[A]
case class Branch[A] (left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  //Excercise 2

  def size[A] (t :Tree[A]): Int = t match {
      case Leaf(_) => 1
      case Branch(l,r) => (size(l) + size(r)) + 1
    }


  // Exercise 3 (3.26)

  def maximum (t: Tree[Int]): Int = t match {
    case Leaf(n)      => n
    case Branch(l, r) => maximum(l) max maximum(r)
  }

  // Exercise 4 (3.28)
  // note sure about the answer
  def map[A,B] (t: Tree[A]) (f: A => B): Tree[B] = t match {
    case Leaf(n) => Leaf(f(n))
    case Branch(l, r) =>  Branch(map(l)(f),map(r)(f))
  }

  // Exercise 5 (3.29)

  // f => Branch g => Leaf
  def fold[A,B] (t: Tree[A]) (f: (B,B) => B) (g: A => B): B = t match {
    case Leaf(n) => g(n)
    case Branch(l,r) => f(fold(l)(f)(g),fold(r)(f)(g))

  }
  def size1[A] (t: Tree[A]): Int = fold[A,Int](t)( (l,r) => (l + r) + 1 )( (x) => 1)

  def maximum1[A] (t: Tree[Int]): Int = fold[Int,Int](t)( (l,r) => l max r)( (x) => x)

  def map1[A,B] (t: Tree[A]) (f: A=>B): Tree[B] = fold[A,Tree[B]](t)( (l,r) => Branch(l,r))( x =>  Leaf(f(x)))


}

sealed trait Option[+A] {

  // Exercise 6 (4.1)

  def map[B] (f: A=>B): Option[B] = this match {
    case None => None
    case Some(x) => Some(f(x))
  }

  // You may Ignore the arrow in default's type below for the time being.
  // (it should work (almost) as if it was not there)
  // It prevents the argument "default" from being evaluated until it is needed.
  // So it is not evaluated in case of Some (the term is 'call-by-name' and we
  // should talk about this soon).

  def getOrElse[B >: A] (default: => B): B = this match {
    case None => default
    case Some(x) => x
  }

  def flatMap[B] (f: A=>Option[B]): Option[B] = this match{
    case None => None
    case Some(x) => f(x)
  }

  def filter (p: A => Boolean): Option[A] = this match{
    case Some(x) => if( p(x) ) this else None
    case _ => None
  }

}

case class Some[+A] (get: A) extends Option[A]
case object None extends Option[Nothing]

object ExercisesOption {

  // Remember that mean is implemented in Chapter 4 of the text book

  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  // Exercise 7 (4.2)
  // we need `m`(mean) and `x`(each element in sequence)
  // we're using flatMap instead of map because mean returns Option[Double] and not Double
  def variance(xs: Seq[Double]): Option[Double] = mean(xs).flatMap(m => mean(xs.map(x => math.pow(x - m, 2))))

  // Exercise 8 (4.3)

  def map2[A, B, C](ao: Option[A], bo: Option[B])(f: (A, B) => C): Option[C] =
  for {
    aoo <- ao
    boo <- bo
  } yield (f(aoo, boo))


  // Exercise 9 (4.4)
  def sequence[A] (aos: List[Option[A]]): Option[List[A]] = aos.foldRight[Option[List[A]]](Some(Nil))( (h,t) => map2(h,t)( (a,b) => (a::b)))

  // Exercise 10 (4.5)

  def traverse[A,B] (as: List[A]) (f :A => Option[B]): Option[List[B]] = as.foldRight[Option[List[B]]](Some(Nil))((h,t) => map2(f(h),t)(_ :: _))

}
