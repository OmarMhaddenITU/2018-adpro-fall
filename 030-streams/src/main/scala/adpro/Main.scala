// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen

package adpro
import Stream._


//See "Main" as a Java-like "main" method.
object Main extends App {

  println("Welcome to Streams, the ADPRO 030 class!!")

  val l1 :Stream[Int] = empty

  val l2 :Stream[Int]= cons(1, cons(2, cons (3, empty)))

  println (l1.headOption)
  println (l2.headOption)
  println(l2)

  val naturals: Stream[Int] = from(0)

  // Terminates fast because streams use lazy evaluation and so really only
  // have to make 10 evaluations when changing the Stream to the List.
  println(naturals.take(1000000000).drop(41).take(10).toList)

  // Again since streams are lazily evaluated this evaluation will first happen
  // when we call toList at which point we only need 50 evaluations.
  println(naturals.takeWhile(_ < 1000000000).drop(100).take(50).toList)

  //println(naturals.forAll(_ >= 0))

  println(naturals.take(10).map(a => a+1).toList)
  println(naturals.take(10).filter(a => a == 1).toList)
}
